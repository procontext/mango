<?php

namespace Procontext\MangoApi\Exception;

use Throwable;

class MangoApiDisabledException extends MangoApiException
{
    public function __construct($message = "Mango API отключен", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}