<?php

namespace Procontext\MangoApi;

use Procontext\MangoApi\Exception\MangoApiDisabledException;
use Procontext\MangoApi\Exception\MangoApiResponseException;

class MangoApi
{
    const REQUEST_URL = 'https://www.mango-office.ru/api/v1/external_form/';

    const RESPONSE_SUCCESS = 'success';
    const RESPONSE_FAILED = 'failed';

    private $enable;

    public function __construct()
    {
        $this->enable = env('MANGO_ENABLE', false);
    }

    /**
     * @return array
     *    [
     *      "status"  => "success"
     *    ]
     */
    public function send(MangoApiParams $params): array
    {
        if(!$this->isEnable()) {
            throw new MangoApiDisabledException();
        }

        $curl = curl_init(self::REQUEST_URL);
        curl_setopt_array($curl, [
            CURLOPT_POST => true,
            CURLOPT_REFERER => $params->referer,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => http_build_query($params->export()),
            CURLOPT_CONNECTTIMEOUT => 30
        ]);

        $response = json_decode(curl_exec($curl), true);
        $response = isset($response) && !empty($response)
            ? $response :
            ['status' => self::RESPONSE_FAILED, 'reason' => curl_error($curl)];

        curl_close($curl);
        
        $status = isset($response['status']) ? $response['status'] : self::RESPONSE_FAILED;

        if($status !== self::RESPONSE_SUCCESS) {
            throw new MangoApiResponseException($response);
        }

        return $response;
    }

    public function isEnable(): bool
    {
        return $this->enable;
    }
}