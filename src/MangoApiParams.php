<?php

namespace Procontext\MangoApi;

use Procontext\MangoApi\Exception\ValidationException;
use \ReflectionClass;
use \ReflectionProperty;

class MangoApiParams
{
    const NOTIFY_TYPE_SMS = 'sms';
    const NOTIFY_TYPE_EMAIL = 'email';

    /**
     * Код мероприятия (обязательный)
     *
     * @var string
     */
    public $form_code;

    /**
     * Сайт с которого отправлен запрос c utm метками
     *
     * @var string
     */
    public $referer;

    /**
     * Имя пользователя
     *
     * @var string
     */
    public $name = '';

    /**
     * Фамилия пользователя
     *
     * @var string
     */
    public $last_name = '';

    /**
     * Регион
     *
     * @var string
     */
    public $region = '';

    /**
     * Компания
     *
     * @var string
     */
    public $company = '';

    /**
     * Должность
     *
     * @var string
     */
    public $position = '';

    /**
     * Адрес сайта компании пользователя
     *
     * @var string
     */
    public $site = '';

    /**
     * Email адрес
     *
     * @var string
     */
    public $email = '';

    /**
     * Телефон в формате +7(XXX)XXX-XX-XX
     *
     * @var string
     */
    public $phone = '';

    /**
     * Является ли пользователь клиентом mango
     *
     * @var string
     */
    public $is_client = '';

    /**
     * Допустимые значения "email", "sms"
     *
     * @var array
     */
    public $notify_type = [];

    protected $properties;
    protected $errors;

    public function __construct(array $formData)
    {
        if(!$this->form_code = env('MANGO_FORM_CODE')) {
            $this->errors[] = 'Код мероприятия не задан в конфигурации';
        }

        $formData = $this->preload($formData);
        $reflection = new ReflectionClass(get_called_class());
        $this->properties = array_column($reflection->getProperties(ReflectionProperty::IS_PUBLIC), 'name');
        foreach ($this->properties as $property) {
            if (isset($formData[$property])) {
                $this->{$property} = $formData[$property];
            } elseif(!isset($this->{$property})) {
                $this->errors[] = "Параметр " . $property . " не задан";
            }
        }

        if($this->errors) {
            throw new ValidationException($this->errors);
        }
    }

    protected function preload(array $formData): array
    {
        $formData['notify_type'] = [];
        if(isset($formData['notifications_email']) && !empty($formData['notifications_email'])) {
            $formData['notify_type'][] = self::NOTIFY_TYPE_EMAIL;
        }
        if(isset($formData['notifications_sms']) && !empty($formData['notifications_sms'])) {
            $formData['notify_type'][] = self::NOTIFY_TYPE_SMS;
        }
        if(isset($formData['mango_client'])) {
            $formData['is_client'] = $formData['mango_client'] === true ? 'Y' : 'N';
        }
        if(isset($formData['site_link'])) {
            $formData['site'] = array_get($formData, 'site_link', '');
        }
        return $formData;
    }

    public function export(): array
    {
        $formData = [];
        foreach ($this->properties as $property) {
            if(
                empty($this->{$property})
                || $property == 'referer'
            ) {
                continue;
            }
            $formData[$property] = $this->{$property};
        }
        return $formData;
    }

    public function errors(): array
    {
        return $this->errors;
    }
}
